import logo from "./logo.svg";
import { useEffect,useState } from "react";
import "./App.css";
import "./style.css"
import { jsPDF } from "jspdf";

const Page1 = ({pdf}) => {
  useEffect(() => {
    
  }, []);
  return (

    <section id="page" className="m-auto" style={{height: pdf.internal.pageSize.getHeight(), width: pdf.internal.pageSize.getWidth()}}>
      <div id="frame1" className="m-auto position-relative d-flex justify-content-center">
        <div className="position-absolute w-100 h-100 p-3" style={{top: 0, left: 0, zIndex: 2}}>
          <div id="top-content" className="row justify-content-between m-0">
            <div className="col-2 text-center p-0 border-1-s-b">
              <p className="m-0 no-of TimeNewRoman fs-7d5px">No. of Certificate</p>
              <p className="m-0 no-num TimeNewRoman">R - 1</p>
            </div>
            <div className="col-2 text-center p-0 border-1-s-b">
              <p className="m-0 no-of TimeNewRoman fs-8px">No. of Shares</p>
              <p className="m-0 no-num TimeNewRoman">[No.]</p>
            </div>
          </div>
          <div id="logo" className="text-center"><img src="./logo.png" alt=""/>
          </div>
          <div id="content" className="container-fluid">
            <div id="company-name" className="row justify-content-center MonotypeCorsiva">
              <div className="col-10 text-center">
                <p className="m-0 font-weight-normal font-italic cpn-name">[Company Name]</p>
                <p className="m-0 cpn-des fs-8px TimeNewRoman">Incorporated under the International Business Companies Act, Chapter 270 of the Laws of Belize, R. E. 2011</p>
                <p className="m-0 cpn-des fs-8px TimeNewRoman">Capital of US [total capital] divided into [total no. of shares] shares of US$[par Value] each.</p>
              </div>
            </div>
            <div id="main-content" className="row MonotypeCorsiva p-2">
              <div className="col-12">
                <p className="m-0 lh-1-dot-1" style={{textIndent: '33px'}}><strong>This is to Certify that</strong> [Shareholder’s Name] is the registered proprietor of</p>
                <p className="m-0 lh-1-dot-1"><span>Five Hundred Ordinary</span> Share(s) of <span>US $1.00</span> each in the above-named Company, numbered <span>1</span> to <span>500</span> inclusive in the Capital Stock of the Company and subject to the Memorandum and Articles of the Company and the conditions, if any, endorsed hereon.</p>
              </div>
              <div className="col-12 px-4 py-1">
                <p className="m-0 lh-1-dot-1" style={{textIndent: '20px'}}><strong>In witness whereof, </strong>the said Company has caused this Certificate to be</p>
                <p className="m-0 lh-1-dot-1">signed by its duly authorized officer(s) and its Corporate Seal is to be hereunto affixed this <span>&nbsp;30th &nbsp;</span> day of.&nbsp;&nbsp;<span>March</span>&nbsp;&nbsp;<span>A.D.&nbsp;&nbsp;2021&nbsp;&nbsp;.</span> (Incorporation date)</p>
              </div>
            </div>
            <div id="footer" className="row justify-content-center MonotypeCorsiva">
              <div id="director" className="col-4 text-left">
                <strong className="fs-8px TimeNewRoman lh-1-dot-1 bt-1-dotted">[Director’s Name]</strong>
                <p className="fs-8px TimeNewRoman lh-1-dot-1">Director</p>
              </div>
            </div>
          </div>
        </div>
        <img className="position-absolute w-100 h-100 top-0" style={{top: 0, left: 0, zIndex: 1}} src="./frame.png" alt="" />
      </div>
    </section>
  );

};

const Page2=({pdf})=>{
  useEffect(() => {
    
  }, []);
  return (

    <section id="page" className="m-auto position-relative" style={{height: pdf.internal.pageSize.getHeight(), width: pdf.internal.pageSize.getWidth()}}>
      <div id="frame" className="m-auto position-relative d-flex justify-content-center">
        <div className="position-absolute w-100 h-100 p-5-3" style={{top: 0, left: 0, zIndex: 3}}>
        <div id="content" className="container-fluid">
        <div id="company-name" className="row justify-content-center MonotypeCorsiva">
              <div className="col-10 text-center">
                <p className="m-0 font-weight-normal font-italic cpn-name">Company Name</p>
              </div>
            </div>
        </div>
        <div id="top-content" className="row justify-content-between m-0 p-0">
            <div className="col-3 text-center p-0 ">
              <p className="m-0 no-of Poppins fs-7d5px">No. of Certificate</p>
              <p className="m-0 MonotypeCorsiva border-1-s-b py-1">1</p>
            </div>
            <div className="col-6 text-center p-0 align-self-end py-1  ">
              <p className="m-0 cpn-des fs-7d5px Poppins">Incorporated in Rebublic of Seychelles</p>
              <p className="m-0 cpn-des fs-7d5px Poppins">under the International Business Companies Act, 2016</p>
            </div>
            <div className="col-3 text-center p-0 ">
              <p className="m-0 no-of Poppins fs-7d5px">No. of Shares</p>
              <p className="m-0 MonotypeCorsiva border-1-s-b py-1">50,000</p>
            </div>
          </div>  
          <div id="contentPoppins"class="row pt-3">
            <div className="col-12">
                <p className="m-0 lh-1-dot-5">THIS IS TO CERTIFY that Shareholder’s Name of</p>
            </div>
            <div className="col-12 text-right">
                <p className="m-0 lh-1-dot-5">Shareholder’s Address is the Registered</p>
            </div>
            <div className="col-12">
              <p className="m-0 lh-1-dot-5">Proprietor of<strong class="MonotypeCorsiva h6">&nbsp;&nbsp;Fifty Thousand (50,000)&nbsp;&nbsp;</strong>Shares of<strong class="MonotypeCorsiva h6">&nbsp;&nbsp;US$1.00&nbsp;&nbsp;</strong>each in the abovenamed Company Subject to the Memorandum and Arrticles of Association thereof.</p>
            </div>
            <div className="col-12">
                <p className="m-0 lh-1-dot-5 pt-3 pb-5">Dated this</p>
            </div>
            <div className="col-4">
              <p className="m-0 lh-1-dot-5 bt-1-dotted">Director’s name</p>
              <p className="m-0 lh-1-dot-5">Director of</p>
                <p className="m-0 lh-1-dot-5">Co. Name</p>
            </div>
            </div>  
        </div>
        
        <img className="position-absolute w-100 h-100 top-0" style={{top: 0, left: 0, zIndex: 2}} src="./Frame84.png" alt="" />      
      </div>
      <img className="position-absolute w-100 h-100 top-0" style={{top: 0, left: 0, zIndex: 1}} src="./background_frame84.png" alt="" />
    </section>
  );
};
const Page3 = ({ pdf }) => {
  useEffect(() => {

  }, []);
  return (

    <section id="page" className="m-auto position-relative" style={{ height: pdf.internal.pageSize.getHeight(), width: pdf.internal.pageSize.getWidth() }}>
      <div id="frame" className="m-auto position-relative d-flex justify-content-center border">
        <div className="position-absolute w-100 h-100 p-5-3 px-5" style={{ top: 0, left: 0, zIndex: 3 }}>
          <div id="content" className="container-fluid">
            <div id="company-name" className="row justify-content-center MonotypeCorsiva">
              <div className="col-10 text-center">
                <p className="m-0 font-weight-normal font-italic cpn-name">Company Name</p>
              </div>
            </div>
          </div>
          <div id="top-content" className="row justify-content-between m-0 p-0">
            <div className="col-2 text-center p-0 ">
              <p className="m-0 no-of Poppins fs-7d5px">No. of Certificate</p>
              <p className="m-0 MonotypeCorsiva border-1-s-b">1</p>
            </div>
            <div className="col-6 text-center p-0 align-self-end ">
              <p className="m-0 cpn-des fs-7d5px Poppins font-italic">Incorporated in Anguilla</p>
              <p className="m-0 cpn-des fs-7d5px Poppins font-italic">under the International Business Companies Act, 2000</p>
            </div>
            <div className="col-2 text-center p-0 ">
              <p className="m-0 no-of Poppins fs-7d5px">No. of Shares</p>
              <p className="m-0 MonotypeCorsiva border-1-s-b">50,000</p>
            </div>
          </div>
          <div id="contentPoppins" class="row pt-3 justify-content-center">
            <div className="col-10">
              <div className="col-12">
                <p className="m-0 lh-1-dot-5 font-weight-bold"><span className="font-italic">THIS IS TO CERTIFY that</span> <span className="font-weight-normal">[Shareholder’s Name]</span> of </p>
              </div>
              <div className="col-12 text-right">
                <p className="m-0 lh-1-dot-5">[Shareholder’s Address] <span className="font-weight-bold">, is</span> the Registered</p>
              </div>
              <div className="col-12">
                <p className="m-0 lh-1-dot-5">Proprietor of <span class="font-weight-bold">Fifty Thousand (50,000)</span> Shares of <span class="font-weight-bold">US$1.00</span> each numbered from <span class="font-weight-bold">1 to 50,000 1 to 50,000</span> inclusive in the above-named Company subject to the Articles of Incorporation and By - Laws thereof.</p>
              </div>
              <div className="col-12">
                <p className="m-0 lh-1-dot-5 pt-3 pb-5 font-weight-bold">Dated this <span className="font-weight-normal">[Incorporation Date]</span></p>
              </div>
            </div>
            <div className="col-12">
              <div className="col-4 p-0">
                <p className="m-0 lh-1-dot-5 bt-1-solid">Director’s name</p>
                <p className="m-0 lh-1-dot-5">Director of</p>
                <p className="m-0 lh-1-dot-5">Co. Name</p>
              </div>
            </div>
            
          </div>
        </div>
      </div>
    </section>
  );
};
function App() {
  const pdf = new jsPDF("p", "px", "a4");
  const size = [pdf.internal.pageSize.getHeight(), pdf.internal.pageSize.getWidth()]
  pdf.deletePage(1);
  pdf.addPage(size, "landscape");
  useEffect(() => {
    console.log(pdf.internal.pageSize.getWidth(), pdf.internal.pageSize.getHeight())
   

    // pdf.html(document.getElementById("page"), {
    //   callback: function (doc) {
    //     doc.deletePage(2);
    //     doc.save();
       
    //   },
    //   x: 0,
    //   y: 0,
    // });
  }, []);
  // return <Page2 pdf={pdf} />;
  return <Page3 pdf={pdf} />;
}

export default App;
